import webview
import sys
import backend.song
from backend.player import Player
from tinytag import TinyTag


if __name__ == "__main__":
    if Player.get_song_count() > 0:
        Player.load_config()

        window = webview.create_window(
            "Music & Lyrics Player v1.4",
            "frontend/index.html",
            width = 800, height = 400,
            min_size=(800, 400),
            js_api=Player()
        )

        webview.start()

        if window.events.closed:
            Player.save_config()
            Player.stop_song()
            window.destroy()
            sys.exit()
    else:
        window = webview.create_window(
            "Error",
            "frontend/error.html",
            js_api=Player()
        )
        webview.start()

        if window.events.closed:
            window.destroy()
            sys.exit()
