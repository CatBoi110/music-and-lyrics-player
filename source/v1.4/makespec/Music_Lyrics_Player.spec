# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['Music_Lyrics_Player.py'],
    pathex=[],
    binaries=[],
    datas=[('backend/player.py', 'backend'), ('backend/song.py', 'backend'), ('frontend/index.html', 'frontend'), ('frontend/error.html', 'frontend'), ('frontend/main.js', 'frontend'), ('frontend/style.css', 'frontend'), ('frontend/max.ico', 'frontend'), ('frontend/min.ico', 'frontend')],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
    optimize=0,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.datas,
    [],
    name='Music_Lyrics_Player',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    upx_exclude=[],
    runtime_tmpdir=None,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon=['player.png'],
)
