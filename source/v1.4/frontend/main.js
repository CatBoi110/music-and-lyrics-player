const playButton = document.querySelector('#play-button')
const pauseButton = document.querySelector('#pause-button')
const stopButton = document.querySelector('#stop-button')
const nextButton = document.querySelector('#next-button')
const previousButton = document.querySelector('#previous-button')
const modeSelector = document.querySelector('#mode-selector')
const songTitle = document.querySelector('#song-title')
const songArtist = document.querySelector('#song-artist')
const lyricsArea = document.querySelector('#lyrics-area')
const volumeSlider = document.querySelector('#volume-slider')
const progressBar = document.querySelector('#progress-bar')
const currentTime = document.querySelector('#time-elapsed')
const maxTime = document.querySelector('#song-duration')
const hideLyricsButton = document.querySelector('#hide-lyrics-button')

playButton.addEventListener('click', playSong)
pauseButton.addEventListener('click', pauseSong)
stopButton.addEventListener('click', stopSong)
nextButton.addEventListener('click', function () { changeSong('next') })
previousButton.addEventListener('click', function () { changeSong('previous') })
progressBar.addEventListener('change', setTime)
modeSelector.addEventListener('change', setMode)
volumeSlider.addEventListener('change', setVolume)

let isPlaying = false
let isPaused = false
let isStopped = false

let songDuration = 0
let functionInstances = 0

window.addEventListener('pywebviewready', async function () {
  loadSong()
  volumeSlider.value = await pywebview.api.get_volume() * 100
  modeSelector.value = await pywebview.api.get_playback_mode()
  setMode()
})

document.addEventListener('keyup', async function (event) {
  switch (event.key) {
    case ' ':
    case 'k':
      if (isPaused === true || isPlaying === false) {
        playSong()
      } else {
        pauseSong()
      }
      break

    case 'ArrowLeft':
    case 'j':
      if (isPlaying) {
        progressBar.value -= 10
        setTime()
      }
      break

    case 'ArrowRight':
    case 'l':
      if (isPlaying) {
        progressBar.value -= -10
        setTime()

        if (progressBar.value >= songDuration) {
          stopSong()
        }
      }
      break

    case 'ArrowUp':
      volumeSlider.value -= -10
      setVolume()
      break

    case 'ArrowDown':
      volumeSlider.value -= 10
      setVolume()
      break

    case '<':
    case ',':
      changeSong('previous')
      break

    case '>':
    case '.':
      changeSong('next')
      break
  }
})

async function loadSong () {
  songTitle.value = ''
  songArtist.value = ''
  lyricsArea.value = ''
  lyricsArea.style.display = "none"
  hideLyricsButton.style.display = "none"

  nextButton.disabled = true
  previousButton.disabled = true

  await pywebview.api.load_song()

  songTitle.value = await pywebview.api.get_title()
  songArtist.value = await pywebview.api.get_artist()

  songDuration = await pywebview.api.get_duration()
  progressBar.max = songDuration
  maxTime.value = formatTime(songDuration)

  getLyrics()

  nextButton.disabled = false
  previousButton.disabled = false
}

async function playSong () {
  functionInstances++
  if (functionInstances === 1) {
    playButton.disabled = true

    isStopped = false
    isPlaying = true

    if (isPaused === true) {
      pywebview.api.unpause_song()
      isPaused = false
    } else {
      await loadSong()

      nextButton.disabled = false
      previousButton.disabled = false

      pywebview.api.save_config()
      pywebview.api.play_song()
    }

    while (isPaused === false && progressBar.value < songDuration) {
      progressBar.value = await pywebview.api.get_song_position()
      currentTime.value = formatTime(progressBar.value)

      if (isStopped === true) {
        stopSong()
        break
      }

      await delay(100)
    }

    if (progressBar.value >= songDuration) {
      currentTime.value = '0:00'
      progressBar.value = 0

      changeSong('next')
    }
  }

  isPlaying = false
  functionInstances--

  playButton.disabled = false
}

async function changeSong (direction = '') {
  stopSong()

  nextButton.disabled = true
  previousButton.disabled = true
  lyricsArea.style.display = 'none'

  pywebview.api.change_song(direction)
  progressBar.value = 0
  currentTime.value = '0:00'

  let previousSong = await pywebview.api.get_previous_song()
  let nextSong = await pywebview.api.get_next_song()

  previousButton.title = previousSong
  nextButton.title = nextSong

  await delay(1000)
  playSong()
}

async function delay (time) {
  return new Promise(resolve => {
    setTimeout(resolve, time)
  })
}

async function getLyrics () {
  lyricsArea.value = await pywebview.api.get_lyrics()

  if (lyricsArea.value === 'Lyrics Not Found' || lyricsArea.value === '') {
    lyricsArea.style.display = 'none'
    hideLyricsButton.style.display = 'none'
  } else {
    lyricsArea.style.display = 'block'
    hideLyricsButton.style.display = 'block'
  }

  lyricsArea.scrollTop = 0
}

function hideLyrics () {
  if (confirm('Do You Want To Disable Searching For Lyrics For This Song?')){
    pywebview.api.blacklist_song()
  }

  lyricsArea.style.display = 'none'
  hideLyricsButton.style.display = 'none'
}

function pauseSong () {
  pywebview.api.pause_song()
  isPaused = true
}

function stopSong () {
  pywebview.api.stop_song()

  currentTime.value = '0:00'
  progressBar.value = 0

  isStopped = true
  isPaused = false
}

function setTime () {
  if (isPlaying) {
    const time = progressBar.value
    currentTime.value = formatTime(time)
    pywebview.api.set_time(time)
  }
}

function formatTime (time) {
  let minutes = Math.floor(time / 60)
  let seconds

  if (minutes > 0) {
    seconds = time - (minutes * 60)
  } else {
    seconds = time
  }

  if (seconds < 10) {
    return minutes + ':0' + seconds
  } else {
    return minutes + ':' + seconds
  }
}

async function selectSong () {
  let title = prompt('Enter Song Title or Press [Enter] To View All Songs:')
  let options = null
  let index = null
  let pages = 1

  if (title !== null) {
    options = Array.from(await pywebview.api.select_song(title))

    if (options.length > 0) {
      if (options.length > 50) {
        pages = options.length / 50

        for (let i = 0; i < pages; i++) {
          index = prompt('Page #' + (i + 1) + '\n' + options.slice(i * 50, (i + 1) * 50).join('') + '\nEnter Song Number (Press [Enter] To Continue To Next Page): ')

          if (index !== '') {
            if (index > options.length || index === 0 || isNaN(index)) {
              alert('Invalid Input. Please Enter A Valid Number.')
              i--
            } else {
              break
            }
          }
        }
      } else {
        while (true) {
          index = prompt(options.join('') + '\nEnter Number:')

          if (index > options.length || index === 0 || isNaN(index)) {
            alert('Invalid Input. Please Enter A Valid Number.')
          } else {
            break
          }
        }
      }
    } else {
      alert('No results found.')
    }

    if (index !== null) {
      await pywebview.api.set_song_index(await pywebview.api.find_song_index(options[index - 1].split(':')[1]))

      stopSong()
      await delay(500)
      await loadSong()
      await delay(500)
      playSong()
    }
  }
}

function setMode () {
  pywebview.api.set_playback_mode(modeSelector.value)
}

function setVolume () {
  pywebview.api.set_volume(volumeSlider.value / 100)
}

function minimizeVolume () {
  pywebview.api.set_volume(0)
  volumeSlider.value = 0
}

function maximizeVolume () {
  pywebview.api.set_volume(100)
  volumeSlider.value = 100
}
