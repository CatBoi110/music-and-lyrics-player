import os
import time
import webview
import random
from pygame import mixer
from backend.song import Song

mixer.init()

CONFIG_FOLDER = "configuration/config.txt"


class Player:
    current_song = None
    song_index = 0
    playback_mode = "sequential"
    volume = 0.5

    @classmethod
    def load_song(cls):
        cls.current_song = Song(cls.song_index)
        mixer.music.load(cls.current_song.path)

    @classmethod
    def play_song(cls):
        cls.load_song()
        mixer.music.play()

        while mixer.music.get_busy():
            time.sleep(1)

    @classmethod
    def change_song(cls, direction="next"):
        if cls.playback_mode == "sequential":
            if direction == "next":
                if cls.song_index == len(Song.SONG_LIST) - 1:
                    cls.song_index = 0
                else:
                    cls.song_index += 1
            elif direction == "previous":
                if cls.song_index == 0:
                    cls.song_index = (cls.song_index - 1) + len(Song.SONG_LIST)
                else:
                    cls.song_index -= 1
        elif cls.playback_mode == "shuffle":
            previous_index = cls.song_index

            while cls.song_index == previous_index:
                cls.song_index = random.randint(0, len(Song.SONG_LIST))
        else:
            cls.song_index = cls.song_index

        cls.current_song = Song(cls.song_index)

    @classmethod
    def get_playback_mode(cls):
        return cls.playback_mode

    @classmethod
    def get_title(cls):
        return cls.current_song.title

    @classmethod
    def get_artist(cls):
        return cls.current_song.artist

    @classmethod
    def get_duration(cls):
        return cls.current_song.duration

    @classmethod
    def get_volume(cls):
        return cls.volume

    @classmethod
    def set_volume(cls, vol):
        cls.volume = vol
        mixer.music.set_volume(cls.volume)

    @classmethod
    def set_playback_mode(cls, mode):
        cls.playback_mode = mode

    @classmethod
    def get_lyrics(cls):
        return cls.current_song.lyrics

    @classmethod
    def save_config(cls):
        file = open(CONFIG_FOLDER, "w")

        file.write(str(Player.current_song.file_name) + "\n")
        file.write(str(Player.playback_mode) + "\n")
        file.write(str(Player.volume))

        file.close()

    @classmethod
    def load_config(cls):
        try:
            file = open(CONFIG_FOLDER, "r")
        except FileNotFoundError:
            if not os.path.exists("configuration"):
                os.makedirs("configuration")
            file = open(CONFIG_FOLDER, "w+")

        values = file.readlines()

        if len(values) == 3:
            for i in range(len(values)):
                values[i] = values[i].replace("\n", "")

                Player.song_index = Song.find_song_index(values[0])
                Player.playback_mode = values[1]
                Player.volume = values[2]

        Player.current_song = Song(Player.song_index)
        mixer.music.set_volume(float(Player.volume))

        file.close()

    def get_song_count():
        return Song.get_song_count()

    def pause_song(self):
        mixer.music.pause()

    def unpause_song(self):
        mixer.music.unpause()

    def stop_song(self):
        mixer.music.stop()

    def get_time(self):
        return round(mixer.music.get_pos() / 1000)

    def set_time(self, time):
        mixer.music.set_pos(float(time))
