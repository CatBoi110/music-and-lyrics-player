import os
import lyricsgenius
from tinytag import TinyTag

API = "NmsaiUJ-lecEQgDvVoLDdJPwYIAL4DFBNClIhQGFXI0wmEhLguCt-INXXBYJOLGE"
FORMATS = [".mp3", ".wav", ".ogg"]

FOLDER_PATH = (os.path.normpath(os.path.join(os.getcwd(), "music")))

if not os.path.exists(FOLDER_PATH):
    os.makedirs("music")


genius = lyricsgenius.Genius(API)
genius.verbose = False


class Song:
    SONG_LIST = sorted(os.listdir(FOLDER_PATH))

    def __init__(self, index):
        self.file_name = Song.SONG_LIST[int(index)]
        self.path = os.path.join(FOLDER_PATH, self.file_name)
        self.get_song_title()
        self.get_song_artist()
        self.get_song_duration()
        self.lyrics = ""

    def get_song_title(self):
        title = ""
        for i in range(len(FORMATS)):
            if FORMATS[i] in self.file_name:
                title = TinyTag.get(self.path).title

                if title == "" or title is None:
                    try:
                        title = self.file_name.split("-")[1].strip()
                        title = Song.remove_extension(title)
                    except IndexError:
                        title = Song.remove_extension(self.file_name)

        self.title = title

    def get_song_artist(self):
        artist = ""
        for i in range(len(FORMATS)):
            if FORMATS[i] in self.file_name:
                artist = TinyTag.get(self.path).artist

                if artist == "" or artist is None:
                    try:
                        artist = self.file_name.split("-")[0].strip()
                        artist = Song.remove_extension(artist)

                        if artist == Song.remove_extension(self.file_name):
                            artist = ""
                    except IndexError:
                        artist = ""

        self.artist = artist

    def get_song_duration(self):
        self.duration = round(TinyTag.get(self.path).duration)

    def get_song_lyrics(self):
        lyrics = "Lyrics Not Found"

        attempts = 1
        timeouts = 0

        title = self.title
        artist = self.artist

        while attempts <= 3 and timeouts < 3:
            if attempts == 2:
                title = Song.filter_text(title)
                artist = Song.filter_text(artist)

            if attempts == 3:
                temp = title
                title = artist
                artist = temp

            if artist != "":
                try:
                    query = genius.search_song(title, artist)
                except Exception as e:
                    timeouts += 1
                    continue
            else:
                try:
                    query = genius.search_song(title)
                except Exception as e:
                    timeouts += 1
                    continue

            if query is not None:
                if title in query.title or artist in query.artist:
                    lyrics = Song.filter_lyrics(query)
                attempts += 1
            else:
                attempts += 1
        self.lyrics = lyrics

    @staticmethod
    def find_song_index(file):
        try:
            return Song.SONG_LIST.index(str(file))
        except ValueError:
            return 0

    @staticmethod
    def get_song_count():
        num = 0

        for i in range(len(Song.SONG_LIST)):
            for extension in FORMATS:
                if extension in Song.SONG_LIST[i]:
                    num += 1
        return num

    @staticmethod
    def remove_extension(file):
        for extension in FORMATS:
            if extension in file:
                file = file.replace(extension, "")
        return file

    @staticmethod
    def filter_text(text):
        oldText = list(text)
        newText = []

        copyChar = True
        for i in range(len(oldText)):
            char = oldText[i]

            if char in ["(", "[", "{"]:
                copyChar = False

            if char in [")", "]", "}"]:
                copyChar = True
                continue

            if copyChar:
                newText.append(char)
        return "".join(newText).strip()

    @staticmethod
    def filter_lyrics(query):
        lyrics = query.lyrics
        title = query.title
        artist = query.artist

        remove_list = [
            "Contributors",
            title + " Lyrics",
            "See " + artist + " LiveGet tickets as low as ",
            artist,
            "You might also like"
        ]

        for string in remove_list:
            lyrics = lyrics.replace(string, "")

        if "$" in lyrics:
            num = ""
            index = lyrics.index("$") + 1

            for i in range(index, len(lyrics)):
                char = lyrics[i:i+1]
                if char in "0123456789":
                    num += char
                else:
                    break

            lyrics = lyrics.replace(num, "")
            lyrics = lyrics.replace("$", "")

        if "Embed" in lyrics:
            num = ""
            index = lyrics.index("Embed")
            lyrics = lyrics.replace("Embed", "")

            for i in range(len(lyrics[:index]), 0, -1):
                char = lyrics[i-1:i]
                if char in "0123456789":
                    num += char
                else:
                    break

            num = num[::-1]
            lyrics = lyrics.replace(num, "")

        if "]" in lyrics:
            line = lyrics[lyrics.index("["):lyrics.index("]") + 1]

            lyrics = line + "\n" + lyrics[lyrics.index("\n") + 1:]

        return lyrics.strip()
