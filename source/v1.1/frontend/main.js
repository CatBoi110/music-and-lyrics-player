const playButton = document.querySelector('#play-button')
const pauseButton = document.querySelector('#pause-button')
const stopButton = document.querySelector('#stop-button')
const nextButton = document.querySelector('#next-button')
const previousButton = document.querySelector('#previous-button')
const modeSelector = document.querySelector('#mode-selector')
const loopButton = document.querySelector('#loop-button')
const songTitle = document.querySelector('#song-title')
const songArtist = document.querySelector('#song-artist')
const lyricsArea = document.querySelector('#lyrics-area')
const volumeSlider = document.querySelector('#volume-slider')
const progressBar = document.querySelector('#progress-bar')
const currentTime = document.querySelector('#time-elapsed')
const maxTime = document.querySelector('#song-duration')

playButton.addEventListener('click', playSong)
pauseButton.addEventListener('click', pauseSong)
stopButton.addEventListener('click', stopSong)
nextButton.addEventListener('click', function () { changeSong('next') })
previousButton.addEventListener('click', function () { changeSong('previous') })
progressBar.addEventListener('change', setTime)
modeSelector.addEventListener('change', setMode)
volumeSlider.addEventListener('change', setVolume)

let isPlaying = false
let isPaused = false
let isStopped = false

let timeElapsed = 0
let songDuration = 0
let functionInstances = 0

window.addEventListener('pywebviewready', async function () {
  loadSong()
  volumeSlider.value = await pywebview.api.get_volume() * 100
  modeSelector.value = await pywebview.api.get_playback_mode()
  setMode()
})

document.addEventListener('keyup', async function (event) {
  switch (event.key) {
    case ' ':
    case 'k':
      if (isPaused === true || isPlaying === false) {
        playSong()
      } else {
        pauseSong()
      }
      break

    case 'ArrowLeft':
    case 'j':
      if (isPlaying) {
        progressBar.value -= 10
        setTime()
      }
      break

    case 'ArrowRight':
    case 'l':
      if (isPlaying) {
        progressBar.value -= -10
        setTime()

        if (progressBar.value >= songDuration) {
          stopSong()
        }
      }
      break

    case 'ArrowUp':
      volumeSlider.value -= -10
      setVolume()
      break

    case 'ArrowDown':
      volumeSlider.value -= 10
      setVolume()
      break

    case '<':
    case ',':
      changeSong('previous')
      break

    case '>':
    case '.':
      changeSong('next')
      break
  }
})

async function loadSong () {
  songTitle.value = ''
  songArtist.value = ''
  lyricsArea.value = ''

  nextButton.disabled = true
  previousButton.disabled = true

  await pywebview.api.load_song()

  songTitle.value = await pywebview.api.get_title()
  songArtist.value = await pywebview.api.get_artist()

  songDuration = await pywebview.api.get_duration()
  progressBar.max = songDuration
  maxTime.value = formatTime(songDuration)

  getLyrics()

  nextButton.disabled = false
  previousButton.disabled = false
}

async function playSong () {
  functionInstances++
  if (functionInstances === 1) {
    playButton.disabled = true

    isStopped = false
    isPlaying = true

    if (isPaused === true) {
      pywebview.api.unpause_song()
      isPaused = false
    } else {
      await loadSong()

      timeElapsed = 0

      await pywebview.api.play_song()

      nextButton.disabled = false
      previousButton.disabled = false
    }

    while (isPaused === false && timeElapsed < songDuration) {
      progressBar.value = timeElapsed
      currentTime.value = formatTime(timeElapsed)

      await delay(1000)

      timeElapsed += 1

      if (isStopped === true) {
        stopSong()
        break
      }
    }

    if (timeElapsed === songDuration) {
      currentTime.value = '0:00'
      timeElapsed = 0
      progressBar.value = 0

      changeSong('next')
    }
  }

  isPlaying = false
  functionInstances--

  playButton.disabled = false
}

async function changeSong (direction = '') {
  stopSong()

  nextButton.disabled = true
  previousButton.disabled = true
  lyricsArea.style.display = 'none'

  pywebview.api.change_song(direction)
  progressBar.value = 0

  let previousSong = await pywebview.api.get_previous_song()
  let nextSong = await pywebview.api.get_next_song()

  previousButton.title = previousSong
  nextButton.title = nextSong

  await delay(1000)
  playSong()
}

async function delay (time) {
  return new Promise(resolve => {
    setTimeout(resolve, time)
  })
}

async function getLyrics () {
  lyricsArea.value = await pywebview.api.get_lyrics()

  if (lyricsArea.value === 'Lyrics Not Found') {
    lyricsArea.style.display = 'none'
  } else {
    lyricsArea.style.display = 'block'
  }

  lyricsArea.scrollTop = 0
}

function pauseSong () {
  pywebview.api.pause_song()
  isPaused = true
}

function stopSong () {
  pywebview.api.stop_song()

  currentTime.value = '0:00'
  progressBar.value = 0

  timeElapsed = 0

  isStopped = true
}

function setTime () {
  if (isPlaying) {
    const time = progressBar.value
    currentTime.value = formatTime(time)
    pywebview.api.set_time(time)
    timeElapsed = Number(time)
  }
}

function formatTime (time) {
  const minutes = Math.floor(time / 60)
  let seconds

  if (minutes > 0) {
    seconds = time - (minutes * 60)
  } else {
    seconds = time
  }

  if (seconds < 10) {
    return minutes + ':0' + seconds
  } else {
    return minutes + ':' + seconds
  }
}

function setMode () {
  pywebview.api.set_playback_mode(modeSelector.value)
}

function setVolume () {
  pywebview.api.set_volume(volumeSlider.value / 100)
}

function minimizeVolume () {
  pywebview.api.set_volume(0)
  volumeSlider.value = 0
}

function maximizeVolume () {
  pywebview.api.set_volume(100)
  volumeSlider.value = 100
}
