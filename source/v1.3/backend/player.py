import os
import time
import webview
import random
from time import sleep
from pygame import mixer
from backend.song import Song

mixer.init()

CONFIG_FILE = "configuration/config.txt"
HIDE_LIST = "configuration/blacklist.txt"


class Player:
    current_song = None
    song_index = 0
    playback_mode = "sequential"
    volume = 0.5
    song_position = 0

    @classmethod
    def load_song(cls):
        cls.current_song = Song(cls.song_index)
        mixer.music.load(cls.current_song.path)

    @classmethod
    def play_song(cls):
        mixer.music.play()
        cls.set_time(cls.song_position)

        while mixer.music.get_busy():
            sleep(1)
            cls.song_position = cls.song_position + 1

    @classmethod
    def stop_song(cls):
        cls.song_position = 0
        mixer.music.stop()

    @classmethod
    def pause_song(cls):
        mixer.music.pause()

    @classmethod
    def unpause_song(cls):
        mixer.music.unpause()

        while mixer.music.get_busy():
            sleep(1)
            cls.song_position = cls.song_position + 1

    @classmethod
    def change_song(cls, direction="next"):
        mixer.music.unload()

        if cls.playback_mode == "sequential":
            if direction == "next":
                if cls.song_index == len(Song.SONG_LIST) - 1:
                    cls.song_index = 0
                else:
                    cls.song_index += 1
            elif direction == "previous":
                if cls.song_index == 0:
                    cls.song_index = len(Song.SONG_LIST) - 1
                else:
                    cls.song_index -= 1
        elif cls.playback_mode == "shuffle":
            previous_index = cls.song_index

            while cls.song_index == previous_index:
                cls.song_index = random.randint(0, len(Song.SONG_LIST))
        else:
            cls.song_index = cls.song_index

    @classmethod
    def get_song_position(cls):
        return cls.song_position

    @classmethod
    def get_playback_mode(cls):
        return cls.playback_mode

    @classmethod
    def get_title(cls):
        return cls.current_song.title

    @classmethod
    def get_artist(cls):
        return cls.current_song.artist

    @classmethod
    def get_duration(cls):
        return cls.current_song.duration

    @classmethod
    def get_volume(cls):
        return cls.volume

    @classmethod
    def select_song(cls, title):
        return Song.select_song(title)

    @classmethod
    def find_song_index(cls, song):
        return Song.find_song_index(song)

    @classmethod
    def set_song_index(cls, index):
        cls.song_index = index

    @classmethod
    def get_lyrics(cls):
        blacklist = cls.get_blacklist()

        for i in range(len(blacklist)):
            if cls.current_song.file_name in blacklist[i]:
                return "Lyrics Not Found"

        cls.current_song.get_song_lyrics()
        return cls.current_song.lyrics

    @classmethod
    def get_blacklist(cls):
        try:
            file = open(HIDE_LIST, "r")
        except FileNotFoundError:
            if not os.path.exists("configuration"):
                os.makedirs("configuration")
            file = open(HIDE_LIST, "w+")

        return file.readlines()

    @classmethod
    def blacklist_song(cls):
        file = open(HIDE_LIST, "a")

        file.write(cls.current_song.file_name + "\n")

    @classmethod
    def get_previous_song(cls):
        if cls.song_index == 0:
            index = len(Song.SONG_LIST) - 1
        else:
            index = cls.song_index - 1

        return Song(index).title

    @classmethod
    def get_next_song(cls):
        if cls.song_index == len(Song.SONG_LIST) - 1:
            index = 0
        else:
            index = cls.song_index + 1

        return Song(index).title

    @classmethod
    def set_volume(cls, vol):
        cls.volume = vol
        mixer.music.set_volume(cls.volume)

    @classmethod
    def set_time(cls, time):
        cls.song_position = int(time)
        mixer.music.set_pos(float(cls.song_position))

    @classmethod
    def set_playback_mode(cls, mode):
        cls.playback_mode = mode

    @classmethod
    def get_song_count(cls):
        return Song.get_song_count()

    @classmethod
    def save_config(cls):
        file = open(CONFIG_FILE, "w")

        file.write(str(cls.current_song.file_name) + "\n")
        file.write(str(cls.playback_mode) + "\n")
        file.write(str(cls.volume) + "\n")
        file.write(str(cls.song_position))

        file.close()

    @classmethod
    def load_config(cls):
        try:
            file = open(CONFIG_FILE, "r")
        except FileNotFoundError:
            if not os.path.exists("configuration"):
                os.makedirs("configuration")
            file = open(CONFIG_FILE, "w+")

        values = file.readlines()

        if len(values) == 4:
            for i in range(len(values)):
                values[i] = values[i].replace("\n", "")

                cls.song_index = Song.find_song_index(values[0])
                cls.playback_mode = values[1]
                cls.volume = values[2]
                cls.song_position = int(values[3])

        cls.current_song = Song(cls.song_index)
        mixer.music.set_volume(float(cls.volume))

        file.close()
