import webview
import sys
import backend.song
from backend.player import Player
from tinytag import TinyTag


if __name__ == "__main__":
    if Player.get_song_count() > 0:
        Player.load_config()

        window = webview.create_window(
            "Music & Lyrics Player v1.3",
            "frontend/index.html",
            min_size=(950, 950),
            js_api=Player()
        )

        webview.start()

        if window.events.closed:
            Player.save_config()
            Player.stop_song()
            window.destroy()
            sys.exit()
    else:
        window = webview.create_window(
            "Error",
            "frontend/error.html",
            js_api=Player()
        )
        webview.start()

        if window.events.closed:
            window.destroy()
            sys.exit()
