# Music & Lyrics Player

<b>MP3 Player And Song Lyrics Finder<b>

![Picture](photos/pic.png)

## Program description

This program can play music files, while also searching the web for the song's lyrics. Using the metadata and name of the song, this program can find most song lyrics and display them in real time. Currently this program supports the following file formats: MP3, OGG, and WAV. 

## Program Installation

### Windows
1. Download the exectuable for your system from the releases tab.
2. Create a folder for the program.
3. Run the program once to create any neccesary folders. Once the program displays a message saying you have no songs in your libary, you may close the program.
4. Place any supported music files in the ```music``` folder.
5. Re-run the program.
6. Enjoy!

### Linux
(Due to file size limitations a pre-built exectuable is unable to be provided instead you may build one on your system)

#### From Source
1. Download the ```source``` folder from the relases page or repository.
2. Ensure all python dependencies are installed, if not type ```pip install -r requirements.txt```.
3. If you want to run the program directly from the python file, type ```python Music_Lyrics_Player.py```.
4. Run the program once to create any neccesary folders. Once the program displays a message saying you have no songs in your libary, you may close the program.
5. Place any supported music files in the ```music``` folder.
6. Re-run the program.
7. Enjoy!


#### Build Exectuable
1. Download the ```source``` folder from the relases page or repository.
2. Ensure all python dependencies are installed, if not type ```pip install -r requirements.txt```.
3. If you want to build the program into a portable executable, then first download pyinstaller (https://pyinstaller.org/en/stable/)
4. Then copy the .spec file into the root of the ```source``` folder.
5. Type ```pyinstaller Music_Lyrics_Player.spec``` to build the exectuable
6. Move the executable file from the ```dist``` folder to a location of your choice
7. Run the executable to start the program
8. Run the program once to create any neccesary folders. Once the program displays a message saying you have no songs in your libary, you may close the program.
9. Place any supported music files in the ```music``` folder.
10. Re-run the program.
11. Enjoy!

## Controls
* ```Play``` (Space or k): Starts playback of current song.
* ```Pause``` (Space or k): Pauses song at current moment.
* ```Stop```: Stops playback and resets song position.
* ```Next/Previous Song ```(< or >): Move to next or previous song in folder.
* ```Mode Select``` (Sequential, Shuffle, Loop): Change means of changing songs, next in alphabetical order, randomly, or repeating same song.
* ```Playback Slider``` (Left or right arrow key): Skip fowards or backwards through song.
* ```Volume Slider``` (Up or down arrow key): Change playback volume up or down.
* ```Select Song```: View folder of songs and search for specific song.
* ```Hide Lyrics```: Hides song lyrics and can add song to blacklist for lyric searching.

## Features
* Simple and minimal user interface
* Player various types of music files
* Automaticly search for lyrics on each song played
* Saves last played song and various user settings
* Portable and easy to run

## Music Lyrics Finder Usage

To ensure the highest chance of finding the correct lyrics for a song. Follow the following guidelines:

1. Remove any unnecessary characters or words from song's title
2. Ensure the metadata for the file is correct. (Title and Artist are correct)
3. If no metadata is found, follow the naming scheme of: ```Artist - Title```.
4. Search for song on `genius.com` to see proper naming scheme.
5. Check to make sure device is connected to a stable internet source.

## Credits
Libraries used:
* `https://www.pygame.org/`
* `https://pypi.org/project/lyricsgenius/`
* `https://pywebview.flowrl.com/`
* `https://pypi.org/project/tinytag/`

<b> (Any songs featured are used for demonstrational purposes only) <b>